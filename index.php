<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "tck";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT * FROM users");
  $stmt->execute();

  // set the resulting array to associative
  $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  $records = $stmt->fetchAll();
  
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
echo "<pre>";
//print_r($records);

?>

<table border=1>
	<thead>
		<th>Id</th>
		<th>Name</th>
		<th>Age</th>
		<th>City</th>
	</thead>
	<tbody>
		<?php foreach ($records as $rec) { ?> 
		<tr>
			<td><?php echo $rec['id']; ?></td>
			<td><?php echo $rec['name']; ?></td>
			<td><?php echo $rec['age']; ?></td>
			<td><?php echo $rec['city']; ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
